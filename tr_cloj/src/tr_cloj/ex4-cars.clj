(defn cars-per-hour [speed]
 (* speed 221.0)
)


(defn production-rate [speed]
  (cond
    (= speed 0) 0.0
    (< speed 5) (cars-per-hour speed)
    (< speed 9) (* 0.9 (cars-per-hour speed ))
    (< speed 10) (* 0.8 (cars-per-hour speed ))
    (< speed 11) (* 0.77 (cars-per-hour speed ))
  )
)

(defn working-items [speed]
  (int (/ (production-rate speed) 60))  
)

(println (working-items 6))
