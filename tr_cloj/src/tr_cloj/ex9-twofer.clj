(defn two-fer 
  ([name] (format "One for %s, one for me." name))
  ([] (two-fer "you"))
)

(println (two-fer "marco"))
(println (two-fer))
