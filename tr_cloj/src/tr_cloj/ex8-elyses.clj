(def deck [5 9 7 1 8])

(defn first-card [deck]
  (first deck)
)

(defn second-card [deck]
  (second deck)  
)

(defn swap-top-two-cards [deck]
  (let  [[f & r] deck]
    (flatten (into [] (concat [(first r) f] (rest r)))))
)

(defn discard-top-card [deck]
  (let [[f & r] deck]
    (concat [f r]))
)

(def face-cards
  ["jack" "queen" "king"])

;(defn insert-face-cards
;  "Returns the deck with face cards between its head and tail."
;  [deck]
;  (if (= (count deck) 0) face-cards
;  (let [[f & r] deck]
;    (if (> (count r) 0)
;              (flatten (into [] (concat [f face-cards r])))
;              (flatten (into [] (concat [f face-cards]))))))
;)

(defn insert-face-cards
    [deck]
    (let [[head & tail] deck]
    (vec (remove nil? (flatten [head face-cards tail]))))
)

(println (insert-face-cards [9]))
