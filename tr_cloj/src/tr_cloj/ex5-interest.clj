(def balance 200.75M)
(def tax-free-percentage 2.5)

(defn interest-rate [balance]
  (cond
    (< balance 0.0M) -3.213
    (< balance 1000.0M) 0.5
    (< balance 5000.0M) 1.621
    :else 2.475
  )  
)

(defn annual-incr [balance]
  (let [multip (bigdec (/ (interest-rate balance) 100))]
  (* (if (neg? balance) (- balance) balance) multip)) 
)

(defn annual-balance-update [balance]
  (+ balance (annual-incr balance))  
)

(defn amount-to-donate [balance tax-free-percentage]
  (if (> balance 0.0M)
    (int (* balance (* 2.0 (/ tax-free-percentage 100.0))))
    0)
)

; (println (interest-rate balance))
; (println (annual-balance-update balance))
; (println (amount-to-donate 550.5M 2.5))
(println (annual-balance-update -0.123M))
