(ns armstrong-numbers)
;; Do (base ** exp)

(defn raise [exp base] 
  ;;(Math/pow base exp) ;; This uses Double, failing.
  ;;(reduce * (repeat exp base))
  (.pow (bigdec base) exp)
)

;; 1234 -> '(1 2 3 4)
(defn digits [number] 
  ; (map #(Character/digit % 10) (str number)))
   (map #(Character/getNumericValue %) (str number))
)
(defn pow-digits [exp arr]
  (map (partial raise exp) arr))
(defn sum-powers [exp n]
  (apply + (pow-digits exp (digits n))) 
)
(defn armstrong? [num]
  (let [exp (count (digits num))] 
    (== (sum-powers exp num) num) 
  ) 
)
