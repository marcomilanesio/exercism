(def knight-awake? false)

(def archer-awake? true)

(def prisoner-awake? false)

(def dog-present? false)

(defn can-fast-attack? [knight-awake]
   (false? knight-awake?)
)

(defn can-spy? [knight-awake archer-awake prisoner-awake]
   (or (or knight-awake? archer-awake?) prisoner-awake?)
)

(defn can-signal-prisoner? [archer-awake prisoner-awake]
  (and (true? prisoner-awake?) (false? archer-awake?))
)

(defn can-free-prisoner? [knight-awake archer-awake prisoner-awake dog-present]
  (if dog-present? (false? archer-awake?) (and (true? prisoner-awake?) (and (false? archer-awake?) (false? knight-awake?))))
)

(println (can-fast-attack? knight-awake?))

(println (can-spy? knight-awake? archer-awake? prisoner-awake?))
(println (can-signal-prisoner? archer-awake? prisoner-awake?))
(println (can-free-prisoner? knight-awake? archer-awake? prisoner-awake? dog-present?))
