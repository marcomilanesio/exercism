(require '[clojure.string :as str])

(defn message [s]
  (str/trim (str/trim-newline (last (str/split s #"\[\w+\]:\ "))))     
)

(defn log-level [s]
  (str/lower-case (str/replace (first (str/split s #":" )) #"\[|\]" ""))
)

(defn reformat [s]
  (format "%s (%s)" (message s) (log-level s))
)


(println (reformat "[WARNING]:  Disk almost full\r\n"))
